(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.mp2 = {
    attach: function (context, settings) {
      var card = new Card({
        // a selector or DOM element for the form where users will
        // be entering their information
        form: '.commerce-checkout-flow',
        // a selector or DOM element for the container
        // where you want the card to appear
        container: '.card-wrapper',
        formSelectors: {
          numberInput: 'input#edit-card-number',
          expiryInput: 'select#edit-expiry-month, select#edit-expiry-year',
          cvcInput: 'input#edit-card-security-code',
          nameInput: 'input#edit-card-holder-name'
        },
        formatting: true,
        // Strings for translation - optional
        messages: {
          validDate: 'valid\ndate',
          monthYear: 'mm/yy'
        },
        // Default placeholders for rendered fields - optional
        placeholders: {
          number: '•••• •••• •••• ••••',
          name: 'Full Name',
          expiry: '••/••',
          cvc: '•••'
        },
        masks: {
          cardNumber: '•'
        },
      });

      /** Wokaround to make card js work with selects see https://github.com/jessepollak/card/issues/340 */
      var year_selector = 'select#edit-expiry-year';
      var month_selector = 'select#edit-expiry-month';
      $(month_selector).change(function(){
        var year = $(year_selector).val() == '' ? '••' : $(year_selector).val();
        $('.jp-card-expiry').text($(this).val()+'/'+year);
      });
      $(year_selector).change(function(){
        var month = $(month_selector).val() == '' ? '••': $(month_selector).val();
        $('.jp-card-expiry').text(month+'/'+$(this).val());
      });
      $(month_selector).add(year_selector).on('focus', function(){
        $('.jp-card-expiry').addClass('jp-card-focused');
      }).on('blur', function(){
        $('.jp-card-expiry').removeClass('jp-card-focused');
      });

      var formAction = $("#commerce-checkout-flow-multistep-default").attr("action");
      $('.commerce-checkout-flow', context).attr("action", "");
      // Change form values so they work with Payfort API.
      $('.commerce-checkout-flow', context).submit(function(e) {
        $('#edit-actions-next', context).attr('disabled','disabled');
        var cardValues = {};
        cardValues.card_holder_name = $(this).find('#edit-card-holder-name').val();
        cardValues.card_number = $(this).find('#edit-card-number').val().replace(/\s/g, '');
        cardValues.expiry_date = $(this).find('#edit-expiry-year').val() + $(this).find('#edit-expiry-month').val();
        cardValues.card_security_code = $(this).find('#edit-card-security-code').val();
        var postData = {};
        if (settings.commerce_payfort.device_fingerprint) {
          postData.device_fingerprint = $(this).find('#device_fingerprint').val();
        }
        $.ajax({
          url: settings.commerce_payfort.signature_url,
          type: 'POST',
          data: postData,
          async: false,
          success: function(data, textStatus, jQxhr) {
            if (data) {
              var form = $("<form></form>", {
                method: 'post',
                action: formAction,
                id: 'payfort-payment-form',
                style:'display: none;'
              });
              form.append($("<input>", { type: 'submit', id: 'submit' }));
              $.extend(cardValues, data);
              $.each(cardValues, function(k, v) {
                var val = v;
                form.append($('<input>', {
                    type: 'hidden',
                    id: k,
                    name: k,
                    value: v
                }));
              });
              $('body').append(form);
              $('#payfort-payment-form input[type=submit]').click();
            }
          }
        });
        e.preventDefault();
        return false;
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
