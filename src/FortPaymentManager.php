<?php

namespace Drupal\commerce_payfort;

/**
 *
 */
class FortPaymentManager implements FortPaymentManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function calculateSignature($params, $phrase, $sha_type) {
    /**
     * Cleanup some un-needed parameters
     */
    $params = array_diff_key($params, array_fill_keys([
      'r',
      'signature',
      'integration_type',
      '3ds',
    ], ''));
    $sha_string = "";
    ksort($params);
    array_walk($params, function ($val, $key) use (&$sha_string) {
      $sha_string .= "$key=$val";
    });

    return hash($sha_type, $phrase . $sha_string . $phrase);
  }

}
