<?php

namespace Drupal\commerce_payfort\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 *
 */
class MerchantPage2Form extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $payment_gateway_config = $payment_gateway_plugin->getConfiguration();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();

    // Placeholder for card.js container.
    $form['card_wrapper'] = [
      '#type' => 'item',
      '#markup' => '<div class="card-wrapper"></div>',
    ];

    $form['card_holder_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cardholder Name'),
      '#attributes' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('Full name'),
      ],
      '#required' => TRUE,
      '#maxlength' => 128,
      '#size' => 20,
      '#description' => $this->t("Cardholder Name must match what's on the card. Avoid using special characters."),
      // Ensure the correct keys by sending values from the form root.
      '#parents' => ['card_holder_name'],
    ];

    $form['card_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Card number'),
      '#attributes' => [
        'autocomplete' => 'off',
        'placeholder' => '**** **** **** ****',
      ],
      '#required' => TRUE,
      '#maxlength' => 19,
      '#size' => 20,
      // Ensure the correct keys by sending values from the form root.
      '#parents' => ['card_number'],
    ];

    // Expiry date wrapper.
    $form['exp_date_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['exp-date-wrapper'],
      ],
    ];

    $form['exp_date_wrapper']['expiry_month'] = [
      '#type' => 'select',
      '#title' => $this->t('Expiry month'),
      '#options' => [
        '' => $this->t('MM'),
        '01' => '01',
        '02' => '02',
        '03' => '03',
        '04' => '04',
        '05' => '05',
        '06' => '06',
        '07' => '07',
        '08' => '08',
        '09' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12',
      ],
      '#required' => TRUE,
      // Ensure the correct keys by sending values from the form root.
      '#parents' => ['expiry_month'],
    ];

    $years = ['' => $this->t('YY')];
    $current_year = date('Y');
    for ($x = 0; $x <= 10; $x++) {
      $year = substr($current_year, -2);
      $years[$year] = $year;
      $current_year++;
    }

    $form['exp_date_wrapper']['expiry_year'] = [
      '#type' => 'select',
      '#title' => $this->t('Expiry year'),
      '#options' => $years,
      '#required' => TRUE,
      // Ensure the correct keys by sending values from the form root.
      '#parents' => ['expiry_year'],
    ];

    $form['card_security_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CVV'),
      '#attributes' => [
        'autocomplete' => 'off',
        'placeholder' => $this->t('CVV'),
      ],
      '#required' => TRUE,
      '#maxlength' => 4,
      '#size' => 4,
      // Ensure the correct keys by sending values from the form root.
      '#parents' => ['card_security_code'],
    ];

    $authorization_params = $payment_gateway_plugin->prepareTokenizationParams($order, $form);
    $data = [
      'merchant_identifier' => $authorization_params['merchant_identifier'],
      'access_code' => $authorization_params['access_code'],
      'service_command' => $authorization_params['service_command'],
      'merchant_reference' => $authorization_params['merchant_reference'],
      'language' => $authorization_params['language'],
      'return_url' => $authorization_params['return_url'],
      'signature' => $authorization_params['signature'],
    ];

    $form = $this->buildRedirectForm($form, $form_state, $payment_gateway_plugin->getTokenizationUrl(), $data, self::REDIRECT_POST);
    $form['#attached']['library'] = [
      'commerce_payfort/mp2',
    ];
    unset($form['commerce_message']['#markup']);
    $form['#process'][] = [
      get_class($this),
      'processForm',
    ];

    $js_settings = [
      'device_fingerprint' => 0,
      'signature_url' => Url::fromRoute('commerce_payfort.signature', ['commerce_order' => $order->id()])->toString(),
    ];

    if ($payment_gateway_config['device_fingerprint']) {
      $form['device_fingerprint'] = [
        '#type' => 'textarea',
        '#attributes' => [
          'id' => 'device_fingerprint',
          'class' => ['hidden'],
        ],
        '#parents' => ['device_fingerprint'],
      ];
      $js_settings['device_fingerprint'] = 1;
      $form['#attached']['library'][] = 'commerce_payfort/fingerprint';
    }

    $form['#attached']['drupalSettings']['commerce_payfort'] = $js_settings;

    return $form;
  }

  /**
   * Process function to remove extrap form api fields.
   *
   * @param array $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param array $complete_form
   *
   * @return array
   */
  public static function processForm(array $element, FormStateInterface $form_state, array &$complete_form) {
    /* Payfort API breaks if anything extra is sent in the post request ¯\_(ツ)_/¯ */
    $complete_form['actions']['next']['#name'] = '';
    unset($complete_form['form_token']);
    unset($complete_form['form_build_id']);
    unset($complete_form['form_id']);

    return $element;
  }

}
