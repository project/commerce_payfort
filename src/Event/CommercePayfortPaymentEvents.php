<?php

namespace Drupal\commerce_payfort\Event;

/**
 * Defines constants for module specific event hooks.
 */
final class CommercePayfortPaymentEvents {

  /**
   * Name of the event to allow amount modification before checkout preparation.
   *
   * @Event
   *
   * @see \Drupal\commerce_payfort\Event\AlterPaymentAmountEvent
   */
  const ALTER_AMOUNT = 'commerce_payfort.alter_payment_amount';

  /**
   * Name of the event to allow amount modification before API call.
   *
   * @Event
   *
   * @see \Drupal\commerce_payfort\Event\AlterPaymentParamsEvent
   */
  const ALTER_PARAMS = 'commerce_payfort.alter_payment_params';

}
