<?php

namespace Drupal\commerce_payfort;

/**
 *
 */
interface FortPaymentManagerInterface {

  /**
   * Generate signature string.
   *
   * @param array $params
   * @param string $phrase
   * @param string $sha_type
   *
   * @return string
   */
  public function calculateSignature($params, $phrase, $sha_type);

}
