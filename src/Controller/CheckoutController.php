<?php

namespace Drupal\commerce_payfort\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\commerce_checkout\CheckoutOrderManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Psr\Log\LoggerInterface;
use Drupal\commerce_payfort\FortPaymentManagerInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Provides the endpoint for signature generation.
 */
class CheckoutController implements ContainerInjectionInterface {

  /**
   * The checkout order manager.
   *
   * @var \Drupal\commerce_checkout\CheckoutOrderManagerInterface
   */
  protected $checkoutOrderManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The payfort payment manager.
   *
   * @var \Drupal\commerce_payfort\FortPaymentManagerInterface
   */
  protected $paymentManager;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructs a new PaymentCheckoutController object.
   *
   * @param \Drupal\commerce_checkout\CheckoutOrderManagerInterface $checkout_order_manager
   *   The checkout order manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\commerce_payfort\FortPaymentManagerInterface
   *   The payfort payment manager.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   */
  public function __construct(CheckoutOrderManagerInterface $checkout_order_manager, LoggerInterface $logger, FortPaymentManagerInterface $fort_payment_manager, PrivateTempStoreFactory $temp_store_factory) {
    $this->checkoutOrderManager = $checkout_order_manager;
    $this->logger = $logger;
    $this->paymentManager = $fort_payment_manager;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_checkout.checkout_order_manager'),
      $container->get('logger.channel.commerce_payment'),
      $container->get('commerce_payfort.payment_manager'),
      $container->get('tempstore.private')
    );
  }

  /**
   * Provides the "generate signature" page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The rout match.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function generateSignaturePage(Request $request, RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $payment_gateway_config = $payment_gateway_plugin->getConfiguration();
    if ($payment_gateway_config['device_fingerprint']) {
      $device_fingerprint = $request->request->get('device_fingerprint');
      $this->tempStoreFactory->get('commerce_payfort')->set('device_fingerprint', $device_fingerprint);
    }
    $params = $payment_gateway_plugin->prepareTokenizationParams($order);

    return new JsonResponse($params, 200);
  }

  /**
   * Provides the "tokenization" page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The rout match.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function tokenizationPage(Request $request, RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $order->get('payment_gateway')->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();
    $payment_gateway_plugin->onTokenization($order, $request);
  }

}
