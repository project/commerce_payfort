<?php

namespace Drupal\commerce_payfort\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payfort\Event\AlterPaymentAmountEvent;
use Drupal\commerce_payfort\Event\AlterPaymentParamsEvent;
use Drupal\commerce_payfort\Event\CommercePayfortPaymentEvents;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\RounderInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsCreatingOffsitePaymentMethodsInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_payfort\FortPaymentManager;
use Symfony\Component\HttpFoundation\Response;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Provides the Payfort Merchant Page 2.0 integration.
 *
 * @CommercePaymentGateway(
 *   id = "payfort_merchantpage2",
 *   label = "Payfort Merchant Page 2.0",
 *   display_label = "Credit card",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_payfort\PluginForm\MerchantPage2Form",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "mastercard",
 *     "visa",
 *   },
 * )
 */
class MerchantPage2 extends OffsitePaymentGatewayBase implements SupportsCreatingOffsitePaymentMethodsInterface {

  /**
   * The default live environment host for page.
   *
   * @var string
   */
  const HOST_LIVE_PAGE = 'https://checkout.payfort.com/';

  /**
   * The default test environment host for page.
   *
   * @var string
   */
  const HOST_TEST_PAGE = 'https://sbcheckout.payfort.com/';

  /**
   * The default live environment host for API.
   *
   * @var string
   */
  const HOST_LIVE_API = 'https://paymentservices.payfort.com/';

  /**
   * The default test environment host for API.
   *
   * @var string
   */
  const HOST_TEST_API = 'https://sbpaymentservices.payfort.com/';

  /**
   * The Payment Manager.
   *
   * @var \Drupal\commerce_payfort\FortPaymentManager
   */
  protected $paymentManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The price rounder.
   *
   * @var \Drupal\commerce_price\RounderInterface
   */
  protected $rounder;

  /**
   * The tempstore factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * Constructs a new MerchantPage2 object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \GuzzleHttp\Client $http_client
   *   The http client.
   * @param \Drupal\commerce_price\RounderInterface $rounder
   *   The price rounder.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The tempstore factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, FortPaymentManager $payment_manager, EventDispatcherInterface $event_dispatcher, Client $http_client, RounderInterface $rounder, PrivateTempStoreFactory $temp_store_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->paymentManager = $payment_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->httpClient = $http_client;
    $this->rounder = $rounder;
    $this->tempStoreFactory = $temp_store_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_payfort.payment_manager'),
      $container->get('event_dispatcher'),
      $container->get('http_client'),
      $container->get('commerce_price.rounder'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_identifier' => '',
      'access_code' => '',
      'sha_request_phrase' => '',
      'sha_response_phrase' => '',
      'device_fingerprint' => FALSE,
      'check_status' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_identifier'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant identifier'),
      '#default_value' => $this->configuration['merchant_identifier'],
      '#required' => TRUE,
    ];

    $form['access_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access code'),
      '#default_value' => $this->configuration['access_code'],
      '#required' => TRUE,
    ];

    $form['sha_type'] = [
      '#type' => 'select',
      '#title' => $this->t('SHA Type'),
      '#options' => [
        'sha256' => $this->t('SHA-256'),
        'sha512' => $this->t('SHA-512'),
      ],
      '#default_value' => $this->configuration['sha_type'],
    ];

    $form['sha_request_phrase'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SHA Request phrase'),
      '#default_value' => $this->configuration['sha_request_phrase'],
      '#required' => TRUE,
    ];

    $form['sha_response_phrase'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SHA Response phrase'),
      '#default_value' => $this->configuration['sha_response_phrase'],
      '#required' => TRUE,
    ];

    $form['device_fingerprint'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable device fingerprint collection'),
      '#default_value' => $this->configuration['device_fingerprint'],
      '#description' => $this->t('Enable device fingerprint collection.'),
    ];

    $form['check_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Check orders status'),
      '#default_value' => $this->configuration['check_status'],
      '#description' => $this->t('Check uncompleted orders status on Payfort.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['access_code'] = $values['access_code'];
      $this->configuration['merchant_identifier'] = $values['merchant_identifier'];
      $this->configuration['sha_type'] = $values['sha_type'];
      $this->configuration['sha_request_phrase'] = $values['sha_request_phrase'];
      $this->configuration['sha_response_phrase'] = $values['sha_response_phrase'];
      $this->configuration['device_fingerprint'] = $values['device_fingerprint'];
      $this->configuration['check_status'] = $values['check_status'];
    }
  }

  /**
   * Get url for tokenization page.
   *
   * @return string
   *   The tokenization page url.
   */
  public function getTokenizationUrl() {
    return ($this->configuration['mode'] == 'live') ? self::HOST_LIVE_PAGE . 'FortAPI/paymentPage' : self::HOST_TEST_PAGE . 'FortAPI/paymentPage';
  }

  /**
   * Get url for API endpoint.
   *
   * @return string
   *   The API url.
   */
  public function getApiUrl() {
    return ($this->configuration['mode'] == 'live') ? self::HOST_LIVE_API . 'FortAPI/paymentApi' : self::HOST_TEST_API . 'FortAPI/paymentApi';
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    /** Dont save payment method for anonymous users or failed payments. */
    $params = \Drupal::request()->request->all();

    $payment_method->card_type = CreditCard::detectType(intval(str_replace('*', 0, $params['card_number'])))->getId();
    $payment_method->card_number = substr($params['card_number'], -4);
    $payment_method->card_exp_year = substr($params['expiry_date'], 0, 2);
    $payment_method->card_exp_month = substr($params['expiry_date'], -2);
    $payment_method->setRemoteId($params['token_name']);
    $expires = CreditCard::calculateExpirationTimestamp($payment_method->card_exp_month->value, $payment_method->card_exp_year->value);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();

    return $payment_method;
  }

  /**
   * {@inheritdoc}
   */
  public function cancelCreatePaymentMethod(Request $request) {

  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $post_data = [
      'service_command' => 'UPDATE_TOKEN',
      'access_code' => $this->configuration['access_code'],
      'merchant_identifier' => $this->configuration['merchant_identifier'],
      'merchant_reference' => $payment_method->id(),
      'language' => 'en',
      'token_name' => $payment_method->getRemoteId(),
      'token_status' => 'INACTIVE',
    ];

    $post_data['signature'] = $this->paymentManager->calculateSignature($post_data, $this->configuration['sha_request_phrase'], $this->configuration['sha_type']);

    $response = $this->callApi($post_data);
    if (substr($response['response_code'], 2) != '000') {
      \Drupal::logger('commerce_payfort')->error('Unable to delete payment method # @id Error code: @code', ['@id' => $payment_method->id(), '@code' => $response['response_code']]);
      throw new InvalidResponseException("Unable to delete payment method");
    }

    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    /** Performing the create payment request, throws an exception if it fails. */
    $remote_id = $payment_method->getRemoteId();
    $params = $this->merchantPageNotifyFort($payment->getOrder(), $remote_id, FALSE);
    if ($this->validateResponseData($params)) {
      $payment->setState('completed');
      $payment->setRemoteId($params['fort_id']);
      $payment->save();
    }
  }

  /**
   * Validate the data received from PayFort.
   *
   * @param array $params
   *   Data received from PayFort.
   *
   * @return bool
   *   The validation result.
   */
  protected function validateResponseData(array $params) {
    if (empty($params)) {
      throw new PaymentGatewayException("Invalid Response Parameters");
    }
    else {
      $signature = $this->paymentManager->calculateSignature($params, $this->configuration['sha_response_phrase'], $this->configuration['sha_type']);
      /**  Check if valid signature. */
      if ($params['signature'] != $signature) {
        \Drupal::logger('commerce_payfort')->error('Invalid signature.');
        throw new PaymentGatewayException('Invalid signature.');
      }
      else {
        if (substr($params['response_code'], 2) != '000') {
          \Drupal::logger('commerce_payfort')->error('Payment failed: ' . $params['response_code'] . ', ' . $params['response_message']);
          throw new HardDeclineException('Payment failed.', $params['response_code']);
        }
      }
    }
    return TRUE;
  }

  /**
   * Process the payment.
   *
   * @param array $params
   *   Array with data received from PayFort.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface|bool
   *   The payment entity or boolean false if the payment with
   *   this authorization code was already processed.
   */
  protected function processPayment(array $params, OrderInterface $order) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');

    if ($payment_storage->loadByRemoteId($params['fort_id'])) {
      return FALSE;
    }

    $payment = $payment_storage->create([
      'state' => 'completed',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->parentEntity->id(),
      'order_id' => $order->id(),
      'remote_id' => $params['fort_id'],
      'remote_state' => $params['status'],
    ]);
    $payment->setState('completed');
    $payment->save();
    \Drupal::logger('commerce_payfort')->debug('payment saved: ' . time() .  ' ' . serialize($params));
    return $payment;
  }

  /**
   * Processes the "tokenization" request.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   Thrown when the request is invalid or the tokenization failed.
   */
  public function onTokenization(OrderInterface $order, Request $request) {
    try {
      $payfort_params = $request->request->all();
      if (empty($payfort_params)) {
        throw new PaymentGatewayException('Empty response from gateway.');
      }
      $params = $payfort_params;
      $response_signature = $payfort_params['signature'];
      $calculated_signature = $this->paymentManager->calculateSignature($params, $this->configuration['sha_response_phrase'], $this->configuration['sha_type']);
      /**  Check if valid signature. */
      if ($response_signature != $calculated_signature) {
        throw new PaymentGatewayException('Invalid signature.');
      }

      /**  Check if success response code. */
      if (substr($payfort_params['response_code'], 2) != '000') {
        \Drupal::logger('commerce_payfort')->error('Payment failed: ' . $payfort_params['response_code'] . ', ' . $payfort_params['response_message']);
        throw new PaymentGatewayException('Gateway error: ' . $payfort_params['response_message'], $payfort_params['response_code']);
      }

      $host2host_params = $this->merchantPageNotifyFort($order, $payfort_params['token_name']);

      if (!$host2host_params) {
        throw new PaymentGatewayException('Invalid response from paymentApi.');
      }

      $params = $host2host_params;
      $response_signature = $host2host_params['signature'];
      $calculated_signature = $this->paymentManager->calculateSignature($params, $this->configuration['sha_response_phrase'], $this->configuration['sha_type']);
      if ($response_signature != $calculated_signature) {
        throw new PaymentGatewayException('Invalid signature.');
      }

      $response_code = $params['response_code'];
      if ($response_code == '20064' && isset($params['3ds_url'])) {
        /** Redirecting to 3DS URL. */
        $response = new RedirectResponse($params['3ds_url'], RedirectResponse::HTTP_SEE_OTHER, ['content-type' => 'text/html']);
        $response->send();
        die();
      }
      else {
        if (substr($response_code, 2) != '000') {
          \Drupal::logger('commerce_payfort')->error('Payment failed: ' . $params['response_code'] . ', ' . $params['response_message']);
          throw new PaymentGatewayException('Payment failed.');
        }
      }
      /** No need for 3DS redirect, redirect to onReturn and mark as complete. */
      $onreturn_url = Url::fromRoute('commerce_payment.checkout.return', [
        'commerce_order' => $order->id(),
        'step' => 'payment',
      ], [
        'query' => $payfort_params,
      ])->toString();
      return new RedirectResponse($onreturn_url);
    }
    catch (\Exception $e) {
      /** Payment tokenization failed, return to payment and unlock order. */
      $order->unlock()->save();
      \Drupal::messenger()->addError($this->t('There was a problem processing your card.'));
      throw new NeedsRedirectException(Url::fromRoute('commerce_checkout.form', [
        'commerce_order' => $order->id(),
        'step' => 'payment',
      ])->toString());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payfort_params = $request->request->all();
    \Drupal::logger('commerce_payfort')->debug('Return: ' . time() .  ' ' . serialize($payfort_params));
    if ($this->validateResponseData($payfort_params)) {
      if ($order->getCustomerId() && substr($payfort_params['response_code'], 2) == '000') {
        $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
        $payment_method = $payment_method_storage->createForCustomer(
          'credit_card',
          $this->parentEntity->id(),
          $order->getCustomerId(),
          $order->getBillingProfile()
        );
        $payment_method = $this->createPaymentMethod($payment_method, $payfort_params);
        $order->set('payment_method', $payment_method);
      }
      $this->processPayment($payfort_params, $order);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $payfort_params = $request->request->all();
    \Drupal::logger('commerce_payfort')->debug('Notify: ' . time() .  ' ' . serialize($payfort_params));
    /** Hold notify to give redirect a chance to complete order */
    sleep(5);
    try {
      list(, $order_id) = explode('-', $payfort_params['merchant_reference']);
      /** @var \Drupal\commerce_order\entity\OrderInterface $order */
      $order = $this->entityTypeManager->getStorage('commerce_order')->load($order_id);
      if (!$order) {
        \Drupal::logger('commerce_payfort')->error('Invalid order ID from gateway: @reference', ['@reference' => $payfort_params['merchant_reference']]);
        throw new PaymentGatewayException('Invalid order ID from gateway.');
      }
      else {
        if ($order->getState()->getId() == 'completed') {
          return new Response('', 200);
        }
      }
      if ($this->validateResponseData($payfort_params)) {
        if ($order->getCustomerId() && substr($payfort_params['response_code'], 2) == '000') {
          $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
          $payment_method = $payment_method_storage->createForCustomer(
            'credit_card',
            $this->parentEntity->id(),
            $order->getCustomerId(),
            $order->getBillingProfile()
          );
          $payment_method = $this->createPaymentMethod($payment_method, $payfort_params);
          $order->set('payment_method', $payment_method);
        }
        $this->processPayment($payfort_params, $order);
      }
    }
    catch (PaymentGatewayException $e) {
      return new Response('', 200);
    }
  }

  /**
   * Call the payment API for the actual purchase.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param string $remote_id
   * @param bool $new
   *
   * @return bool|mixed
   */
  public function merchantPageNotifyFort(OrderInterface $order, $remote_id, $new = TRUE) {
    $billing_profile = $order->getBillingProfile();
    /** @var \Drupal\address\AddressInterface|null $billing_address */
    $billing_address = $billing_profile
    && $billing_profile->hasField('address')
    && !$billing_profile->get('address')->isEmpty() ?
    $billing_profile->address->first() : NULL;
    $payment_amount = $this->getPayableAmount($order);
    $post_data = [
      'merchant_reference' => $this->getMerchantReference($order),
      'access_code' => $this->configuration['access_code'],
      'command' => 'PURCHASE',
      'merchant_identifier' => $this->configuration['merchant_identifier'],
      'customer_ip' => \Drupal::request()->getClientIp(),
      'amount' => (string) $this->convertFortAmount($payment_amount),
      'currency' => strtoupper($payment_amount->getCurrencyCode()),
      'customer_email' => $order->getEmail(),
      'customer_name' => preg_replace("/[^\p{L}0-9\s\-_'\/\\ ]+/u", '', $billing_address->getGivenName() . ' ' . $billing_address->getFamilyName()),
      'token_name' => $remote_id,
      'language' => 'en',
      'return_url' => $this->prepareReturnUrl($order),
    ];

    if (!$new) {
      $post_data['eci'] = 'RECURRING';
    }
    else {
      /** Send remember me flag for logged in customers **/
      if ($order->getCustomerId()) {
        $post_data['remember_me'] = 'YES';
      }
      /** Send device fingerprint if enabled in config **/
      if ($this->configuration['device_fingerprint']) {
        $post_data['device_fingerprint'] = $this->tempStoreFactory->get('commerce_payfort')->get('device_fingerprint');
      }
    }
    // Allow others to alter API parameters.
    $event = new AlterPaymentParamsEvent($post_data, $order);
    $this->eventDispatcher->dispatch($event, CommercePayfortPaymentEvents::ALTER_PARAMS);
    $post_data = $event->getPaymentParams();

    // Calculate request signature.
    $post_data['signature'] = $this->paymentManager->calculateSignature($post_data, $this->configuration['sha_request_phrase'], $this->configuration['sha_type']);
    \Drupal::logger('commerce_payfort')->debug('Payfort API request: ' . serialize($post_data));
    $result = $this->callApi($post_data);

    return $result;
  }

  /**
   * Send http request to payfort API.
   *
   * @param array $params
   *
   * @return array
   */
  public function callApi($params) {
    $url = $this->getApiUrl();
    try {
      $response = $this->httpClient->post($url, ['json' => $params]);
      $json_response = json_decode($response->getBody(), TRUE);
      if (!$json_response || empty($json_response)) {
        throw new PaymentGatewayException('Empty response from the API');
      }
      return $json_response;
    }
    catch (RequestException $request_exception) {
      throw new PaymentGatewayException('Error occurred on calling the API', $request_exception->getCode(), $request_exception->getMessage());
    }
  }

  /**
   * Prepare tokenization parameters.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   * @param string $type
   *
   * @return array
   */
  public function prepareTokenizationParams(OrderInterface $order, $type = 'request') {
    $params = [
      'merchant_identifier' => $this->configuration['merchant_identifier'],
      'access_code' => $this->configuration['access_code'],
      'merchant_reference' => $this->getMerchantReference($order),
      'service_command' => 'TOKENIZATION',
      'language' => $this->getLanguageCode(),
      'return_url' => $this->prepareTokenizationUrl($order),
    ];
    $phrase = ($type == 'request') ? $this->configuration['sha_request_phrase'] : $this->configuration['sha_response_phrase'];
    $params['signature'] = $this->paymentManager->calculateSignature($params, $phrase, $this->configuration['sha_type']);

    return $params;
  }

  /**
   * Returns the payable amount for the given order.
   *
   * Give other modules the possibility to alter price.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_price\Price
   *   The calculated payment amount to charge.
   */
  public function getPayableAmount(OrderInterface $order) {
    $event = new AlterPaymentAmountEvent($order);
    $this->eventDispatcher->dispatch($event, CommercePayfortPaymentEvents::ALTER_PARAMS);
    $payment_amount = $event->getPaymentAmount();
    return $this->rounder->round($payment_amount);
  }

  /**
   * Convert amount with decimal points.
   *
   * @param float $amount
   *
   * @return float
   */
  public function convertFortAmount($price) {
    $decimal_points = $this->getCurrencyDecimalPoints($price->getCurrencyCode());
    return round($price->getNumber(), $decimal_points) * (pow(10, $decimal_points));
  }

  /**
   * Get unique ID for order to be sent to gateway.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   */
  public function getMerchantReference(OrderInterface $order) {
    $merchant_reference = time() . '-' . $order->id();
    $order->setData('merchant_reference', $merchant_reference);
    $order->save();

    return $merchant_reference;
  }

  /**
   * Get the number of decimal points for the API.
   *
   * @param string $currency
   */
  public function getCurrencyDecimalPoints($currency) {
    $currency = strtoupper($currency);
    $decimal_point = 2;
    $currencies = [
      'JOD' => 3,
      'KWD' => 3,
      'OMR' => 3,
      'TND' => 3,
      'BHD' => 3,
      'LYD' => 3,
      'IQD' => 3,
    ];
    if (isset($currencies[$currency])) {
      $decimal_point = $currencies[$currency];
    }
    return $decimal_point;
  }

  /**
   * Generates the return url.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string|\Drupal\Core\GeneratedUrl
   */
  private function prepareReturnUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], [
      'absolute' => TRUE,
    ])->toString();
  }

  /**
   * Generates the tokenization url.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return string|\Drupal\Core\GeneratedUrl
   */
  public function prepareTokenizationUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payfort.tokenization', [
      'commerce_order' => $order->id(),
    ], [
      'absolute' => TRUE,
    ])->toString();
  }

  /**
   * Returns the 2 letter language code, payfort only support 'en' and 'ar' at this moment.
   *
   * @return string
   */
  private function getLanguageCode() {
    $lang = \Drupal::languageManager()->getCurrentLanguage()->getId();
    return ($lang == 'ar') ? 'ar' : 'en';
  }

}
